#include <Arduino.h>
#include <Servo.h> 

int servoPin = 5; 
Servo Serv; 

void setup() {
  Serial.begin(9600);
  Serv.attach(servoPin); 
  Serial.println("Hello World");
}

void rotateServo(int deg) {
  Serial.print("Moving to ");
  Serial.println(deg);
  Serv.write(deg);
}

void turnLeft() {
  rotateServo(70);
}

void straightenUp() {
  rotateServo(100);
}

void turnRight() {
  rotateServo(130);
}

void loop() {
  straightenUp();
  delay(2000);

  turnLeft(); 
  delay(2000);

  straightenUp();
  delay(2000);

  turnRight();
  delay(2000); 
}